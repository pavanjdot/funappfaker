import React from 'react'
import ReactDOM from 'react-dom'
import faker from 'faker'
import CommentDetails from './CommentDetails'
import ApprovalCard from './ApprovalCard'
import Menu from './Menu'

const App = ()=>{
  return(
    <div className="ui container comments">
      <Menu/>
      <ApprovalCard>
      <CommentDetails name="Pavan" metadata="Today at 5:45PM" text="Awesome blog!" image={faker.image.avatar()}/>
      <CommentDetails name="Vivek" metadata="Today at 3:30PM" text="Not Cool!" image={faker.image.avatar()}/>
      </ApprovalCard>

      <ApprovalCard>
      <CommentDetails name="Pavan" metadata="Today at 5:45PM" text="Awesome blog!" image={faker.image.avatar()}/>
      <CommentDetails name="Vivek" metadata="Today at 3:30PM" text="Not Cool!" image={faker.image.avatar()}/>
      </ApprovalCard>

      <ApprovalCard>
      <CommentDetails name="Pavan" metadata="Today at 5:45PM" text="Awesome blog!" image={faker.image.avatar()}/>
      <CommentDetails name="Vivek" metadata="Today at 3:30PM" text="Not Cool!" image={faker.image.avatar()}/>
      </ApprovalCard>

      

      
     
      
    </div>
  );
}

ReactDOM.render(<App/>, document.getElementById('root'));